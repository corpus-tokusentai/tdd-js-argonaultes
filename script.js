function calculate_area_rectangle(length, width) {
  if (typeof length !== "number" || typeof width !== "number") {
    throw new TypeError("TypeError");
  }
  if (length < 0 || width < 0) {
    throw new RangeError("RangeError");
  }
  return length * width;
}
module.exports = calculate_area_rectangle;
