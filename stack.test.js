const Stack = require("./stack");
const mystack = new Stack();

test("Stack should be an object", () => {
  expect(typeof new Stack()).toBe("object");
});

test("Stack should have a property called push", () => {
  expect(new Stack()).toHaveProperty("push");
});

test("Stack should have a property called pop", () => {
  expect(new Stack()).toHaveProperty("pop");
});

test("Stack should have a property called isEmpty", () => {
  expect(new Stack()).toHaveProperty("isEmpty");
});

test("Stack method push should be a function", () => {
  expect(typeof new Stack().push).toBe("function");
});

test("Stack method pop should be a function", () => {
  expect(typeof new Stack().pop).toBe("function");
});

test("Stack method isEmpty should be a function", () => {
  expect(typeof new Stack().isEmpty).toBe("function");
});

test("Stack method push should add an element to the Stack", () => {
  const mystack = new Stack().push(1);
  expect(mystack.isEmpty()).toBe(false);
});

test("Stack method pop should remove an element from the Stack", () => {
  const mystack = new Stack().push(1);
  mystack.pop();
  expect(mystack.isEmpty()).toBe(true);
});

test("Stack method isEmpty should return true if the Stack is empty", () => {
  expect(new Stack().isEmpty()).toBe(true);
});

test("Stack method isEmpty should return false if the Stack is not empty", () => {
  const mystack = new Stack().push(1);
  expect(mystack.isEmpty()).toBe(false);
});

test("Stack method pop should return the last element added to the Stack", () => {
  const mystack = new Stack().push(1);
  mystack.push(2);
  expect(mystack.pop()).toBe(2);
});

test("Stack method pop should return undefined if the Stack is empty", () => {
  expect(new Stack().pop()).toBe(undefined);
});

test("Stack method pop should return the last element added to the Stack", () => {
  const mystack = new Stack().push(1);
  mystack.push(2);
  mystack.push(3);
  expect(mystack.pop()).toBe(3);
});

test("Stack method pop should return elements in the order they were added to the Stack", () => {
  const mystack = new Stack().push(1);
  mystack.push(2);
  mystack.push(3);
  expect(mystack.pop()).toBe(3);
  expect(mystack.pop()).toBe(2);
  expect(mystack.pop()).toBe(1);
});

test("Item should be an object", () => {
  expect(typeof new Item()).toBe("object");
});
