class Stack {
  #lastItem = null;
  push(value) {
    this.#lastItem = new Item(value, this.#lastItem);
    return this;
  }
  pop() {
    if (this.#lastItem) {
      const value = this.#lastItem.value;
      this.#lastItem = this.#lastItem.reference;
      return value;
    } else return undefined;
  }
  isEmpty() {
    return !this.#lastItem;
  }
}

module.exports = Stack;
class Item {
  #value;
  #reference;
}
