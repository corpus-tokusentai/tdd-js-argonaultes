const calculate_area_rectangle = require("./script");

// make test to check if the function is returning a number
test("Area of rectangle 3 by 2 to be a number", () => {
  expect(typeof calculate_area_rectangle(3, 2)).toBe("number");
});

// make test to check if the function is returning the correct area
test("Area of rectangle 1 by 2 to be 2", () => {
  expect(calculate_area_rectangle(1, 2)).toBe(2);
});

test("Area of rectangle 2 by 2 to be 4", () => {
  expect(calculate_area_rectangle(2, 2)).toBe(4);
});

test("Area of rectangle 3 by 2 to be 6", () => {
  expect(calculate_area_rectangle(3, 2)).toBe(6);
});

//make test to check if negative numbers are not allowed as input values for the function and the function should throw an error ValueError

test("Area of rectangle -3 by 2 to be 0", () => {
  function test() {
    calculate_area_rectangle(-3, 2);
  }
  expect(test).toThrow("RangeError");
});

test("Area of rectangle 3 by -2 to be 0", () => {
  function test() {
    calculate_area_rectangle(3, -2);
  }
  expect(test).toThrow("RangeError");
});

test("Area of rectangle -3 by -2 to be 0", () => {
  function test() {
    calculate_area_rectangle(-3, -2);
  }
  expect(test).toThrowError("RangeError");
});

// make test to check if the function is accepting only a number
test("Area of rectangle 3 by '2' to throw TypeError", () => {
  function test() {
    calculate_area_rectangle(3, "2");
  }
  expect(test).toThrowError("TypeError");
});

test("Area of rectangle '3' by 2 to be 0", () => {
  function test() {
    calculate_area_rectangle("3", 2);
  }
  expect(test).toThrowError("TypeError");
});

test("Area of rectangle '3' by '2' to be 0", () => {
  function test() {
    calculate_area_rectangle("3", "2");
  }
  expect(test).toThrowError("TypeError");
});
